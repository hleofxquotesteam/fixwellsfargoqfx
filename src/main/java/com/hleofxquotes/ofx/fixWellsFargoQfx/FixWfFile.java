package com.hleofxquotes.ofx.fixWellsFargoQfx;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FixWfFile {
    public void execute(File inputFile, File outputFile) throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader(inputFile))) {
            try (BufferedWriter writer = new BufferedWriter(new FileWriter(outputFile))) {
                execute(reader, writer);
            }
        }
    }

    private void execute(BufferedReader reader, BufferedWriter writer) throws IOException {
        StringBuffer headerBuffer = new StringBuffer();
        StringBuffer bodyBuffer = new StringBuffer();

        final String eol = System.lineSeparator();

        readIntoBlocks(reader, eol, headerBuffer, bodyBuffer);

        String headerBlock = toHeaderBlock(headerBuffer, eol);

        String bodyBlock = bodyBuffer.toString();
        
        writeBlocks(eol, headerBlock, bodyBlock, writer);
    }

    protected void writeBlocks(final String eol, String headerBlock, String bodyBlock, BufferedWriter writer) throws IOException {
        // write new header and body block out
        
        // headers
        writer.write(headerBlock);
        writer.write(eol);
        // header and body separator
        writer.write(eol);
        // now body
        writer.write(bodyBlock);
        writer.write(eol);
    }

    protected String toHeaderBlock(StringBuffer headerBuffer, final String eol) {
        String headerBlock = headerBuffer.toString();
        // add end-of-line to header block
        String[] headerKeys = { "DATA:", "VERSION:", "SECURITY:", "ENCODING:", "CHARSET:", "COMPRESSION:", "OLDFILEUID:",
                "NEWFILEUID:", };
        for (String headerKey : headerKeys) {
            headerBlock = headerBlock.replace(headerKey, eol + headerKey);
        }
        return headerBlock;
    }

    protected void readIntoBlocks(BufferedReader reader, final String eol, StringBuffer headerBuffer, StringBuffer bodyBuffer)
            throws IOException {
        boolean header = true;
        char[] buffer = new char[1];        int n = 0;
        while ((n = reader.read(buffer)) != -1) {
            char c = buffer[0];
            if (header) {
                // in header
                if (c == '<') {
                    // see first char of body
                    header = false;
                    bodyBuffer.append(c);
                } else {
                    // still in header
                    headerBuffer.append(c);
                }
            } else {
                // in body
                if (c == '<') {
                    // add newline
                    bodyBuffer.append(eol);
                }
                bodyBuffer.append(c);
            }
        }
    }
}
