package com.hleofxquotes.ofx.fixWellsFargoQfx;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Desktop;
import java.awt.HeadlessException;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import javax.swing.TransferHandler;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

public class FixWellsFargoQfx extends JFrame {
    private static final Logger LOGGER = LogManager.getLogger(FixWellsFargoQfx.class);

    private static final String STATUS_INITIAL_MESSAGE = "Drop QFX file onto target below ...";

    private static final String STATUS_MESSAGE_PREFIX = "Message: ";

    private ExecutorService threadPool = Executors.newSingleThreadExecutor();

    private JLabel status;

    public FixWellsFargoQfx() throws HeadlessException {
        super("Fix Wells Fargo bad QFX - Aug 2018");

        createMainView(getContentPane());
    }

    private void createMainView(Container contentPane) {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        JPanel view = new JPanel();
        // view.setPreferredSize(new Dimension(300, 300));
        view.setLayout(new BorderLayout());
        view.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        ImageIcon icon = null;
        String resourceName = "target.png";
        URL location = getClass().getResource(resourceName);
        LOGGER.info("location=" + location);
        if (location != null) {
            icon = new ImageIcon(location);
        }

        status = new JLabel(STATUS_MESSAGE_PREFIX + STATUS_INITIAL_MESSAGE);
        view.add(status, BorderLayout.NORTH);

        JLabel label = new JLabel();
        view.add(label, BorderLayout.CENTER);
        if (icon != null) {
            label.setIcon(icon);
        } else {
            label.setText("Drop here");
        }

        TransferHandler transferHandler = new FileDropHandler() {

            @Override
            public void handleFile(File inFile) {
                setStatus("> Received file=" + inFile.getAbsolutePath());
                String name = inFile.getName();
                if (name.endsWith(".qfx") || (name.endsWith(".QFX"))) {
                    File outFile = toOutFile(inFile);

                    fixFile(inFile, outFile);
                } else {
                    Runnable doRun = new Runnable() {

                        @Override
                        public void run() {
                            JOptionPane.showMessageDialog(FixWellsFargoQfx.this,
                                    "Don't know how to fix file=" + inFile.getAbsolutePath(), "Invalid suffix",
                                    JOptionPane.ERROR_MESSAGE);

                        }
                    };
                    SwingUtilities.invokeLater(doRun);
                }
            }

            protected File toOutFile(File file) {
                String name = file.getName();

                String prefix = null;
                String suffix = null;
                int index = name.lastIndexOf(".");
                if (index < 0) {
                    // no suffix
                    prefix = name;
                    suffix = null;
                } else {
                    prefix = name.substring(0, index);
                    suffix = name.substring(index);
                }

                String outName = prefix + "-fix" + suffix;
                LOGGER.info("outName=" + outName);
                File outFile = new File(file.getParentFile(), outName);
                return outFile;
            }
        };
        view.setTransferHandler(transferHandler);

        contentPane.add(view);
    }

    protected void fixFile(File inFile, File outFile) {
        // fixFileUsingJython(inFile, outFile);
        fixFileUsingJava(inFile, outFile);
    }

    protected void setStatus(final String string) {
        Runnable doRun = new Runnable() {

            @Override
            public void run() {
                if (status == null) {
                    return;
                }

                status.setText(STATUS_MESSAGE_PREFIX + string);
            }
        };
        SwingUtilities.invokeLater(doRun);

    }

    protected void fixFileUsingJava(final File inputFile, final File outputFile) {
        Runnable command = new Runnable() {

            @Override
            public void run() {
                setStatus("START fixing ....");

                Exception exception = null;

                try {
                    FixWfFile fixWfFile = new FixWfFile();
                    fixWfFile.execute(inputFile, outputFile);
                } catch (Exception e) {
                    exception = e;
                    LOGGER.error(e, e);
                } finally {
                    if (exception != null) {
                        setStatus("DONE fixing with error=" + exception.getMessage());
                        handleError(exception);
                    } else {
                        setStatus("DONE fixing with no error.");
                        handleSucess(outputFile);
                    }
                    LOGGER.info("< DONE");
                }
            }
        };
        threadPool.execute(command);
    }

    protected void handleSucess(final File outFile) {
        Runnable doRun = new Runnable() {

            @Override
            public void run() {
                // doOk(outFile);
//                doYesNo(outFile);
                 doYesNoCancel(outFile);

                setStatus(STATUS_INITIAL_MESSAGE);
            }

            private void doOk(File outFile) {
                Component parentComponent = FixWellsFargoQfx.this;
                String successMessage = "Converted successfully";
                String title = successMessage;
                String message = successMessage + ".\n" + "Converted file is " + outFile.getAbsolutePath() + ".\n"
                        + "It should be in the same folder as the original file.";
                JOptionPane.showMessageDialog(parentComponent, message, title, JOptionPane.INFORMATION_MESSAGE);
            }

            private void doYesNo(final File outFile) {
                Component parentComponent = FixWellsFargoQfx.this;
                String successMessage = "Converted successfully";
                String title = successMessage;
                String message = successMessage + ".\n" + "Converted file is " + outFile.getAbsolutePath() + ".\n"
                        + "It should be in the same folder as the original file" + ".\n"
                        + "Click 'Yes' to open new file; 'No', to close the popup.";
                int input = JOptionPane.showConfirmDialog(parentComponent, message, title, JOptionPane.YES_NO_OPTION);
                // 0=yes, 1=no
                if (input == 0) {
                    LOGGER.info("YES, go ahead a open it.");
                    openFile(outFile, false);
                } else {
                    LOGGER.info("NO, Converted file is " + outFile.getAbsolutePath());
                }
            }

            private void doYesNoCancel(final File outFile) {
                Component parentComponent = FixWellsFargoQfx.this;
                String successMessage = "Converted successfully";
                String title = successMessage;
                String message = successMessage + ".\n" + "Converted file is " + outFile.getAbsolutePath() + ".\n"
                        + "It should be in the same folder as the original file" + ".\n"
                        + "Do you want to open the new file?"
                        ;
                Object[] options = {"Yes, open as QFX",
                        "Yes, open as OFX",
                        "No, just close this window"};
                int input = JOptionPane.showOptionDialog(parentComponent, message, title, JOptionPane.YES_NO_CANCEL_OPTION,
                        JOptionPane.QUESTION_MESSAGE,
                        null,//do not use a custom Icon
                        options,//the titles of buttons
                        options[0]);//default button title
                // 0=yes, 1=no
                if (input == JOptionPane.YES_OPTION) {
                    LOGGER.info("YES, open as QFX.");
                    boolean asOfx = false;
                    openFile(outFile, asOfx);
                } else if (input == JOptionPane.NO_OPTION) {
                    LOGGER.info("YES, open as OFX.");
                    boolean asOfx = true;
                    openFile(outFile, asOfx);
                } else {
                    LOGGER.info("CANCEL, Converted file is " + outFile.getAbsolutePath());
                }
            }

            private void openFile(final File outFile, final boolean asOfx) {
                Runnable command = new Runnable() {

                    @Override
                    public void run() {
                        try {
                            File openAsFile = outFile;
                            if (asOfx) {
                                openAsFile = convertToOfx(outFile);
                            }
                            Desktop desktop = Desktop.getDesktop();
                            LOGGER.info("OPEN file=" + openAsFile.getAbsolutePath());
                            desktop.open(openAsFile);
                        } catch (IOException e) {
                            LOGGER.error(e, e);
                        }
                    }
                };
                threadPool.execute(command);
            }
        };
        SwingUtilities.invokeLater(doRun);
    }

    private File convertToOfx(File outFile) throws IOException {
        String name = outFile.getName();
        int index = name.lastIndexOf(".");
        String prefix = "";
        if (index > 0) {
            prefix = name.substring(0,  index);
        }
        
        File file = new File(outFile.getParentFile(), prefix + ".ofx");
        
        Files.copy(outFile.toPath(), file.toPath(), StandardCopyOption.REPLACE_EXISTING);
        
        return file;
    }
    
    protected void handleError(Exception exception) {
        Runnable doRun = new Runnable() {

            @Override
            public void run() {
                JOptionPane.showMessageDialog(FixWellsFargoQfx.this, exception.getMessage(), "Completed with error",
                        JOptionPane.ERROR_MESSAGE);
            }
        };
        SwingUtilities.invokeLater(doRun);
    }

    public static void main(String[] args) {
        FixWellsFargoQfx app = new FixWellsFargoQfx();
        SwingUtilities.invokeLater(new Runnable() {

            @Override
            public void run() {
                app.showMainFrame();
            }
        });
    }

    private void showMainFrame() {
        setLocationRelativeTo(null);
        pack();
        setVisible(true);
    }

}
